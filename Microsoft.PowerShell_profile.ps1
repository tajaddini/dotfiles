Import-Module posh-git
Import-Module oh-my-posh
Import-Module PowerLine
Set-PowerLinePrompt -PowerLineFont -SetCurrentDirectory -RestoreVirtualTerminal -Colors "#FFDD00", "#FF6600"


[System.Collections.Generic.List[ScriptBlock]]$Prompt = @(
    { $executionContext.SessionState.Path.CurrentLocation }
    { '>' * ($nestedPromptLevel + 1) }
)

Set-Theme Powerlevel10k-Classic

Function touch
{
    $file = $args[0]
    if($file -eq $null) {
        throw "No filename supplied"
    }

    if(Test-Path $file)
    {
        (Get-ChildItem $file).LastWriteTime = Get-Date
    }
    else
    {
        echo $null > $file
    }
}