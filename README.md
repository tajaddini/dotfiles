# Dotfiles and Configurations

## Setting Up The Best Windows Terminal

### Pre-requisites

#### Anaconda

##### Installation

Grab the latest build [here](https://www.anaconda.com/products/individual#Downloads) and install it. I chose to add it to my path and set it up as my default python interpreter. The choice is yours though. We'll need the conda powershell that comes with it for now.

##### Update

It's a wise thing to update your anaconda right off the bat; so, open your start menu and open an anaconda powershell and run something like
`conda update -n base -c defaults conda`.

##### Make an Environment For Your Default Env

Something like `conda create -n sandbox python=3.8 numpy` will do. We will need this later.

#### Windows Terminal

You can either grab the latest release from their [official GitHub repository](https://github.com/microsoft/terminal/releases) or from the [Microsoft Store](https://www.microsoft.com/en-us/p/windows-terminal). Once installed, you can simply merge my configurations with yours.

#### Git

Install [Git for Windows](https://github.com/git-for-windows/git/releases/latest) IMHO.

#### Powerline Patched Fonts

There's this [amazing repository](https://github.com/ryanoasis/nerd-fonts/releases/latest). Go grab one you like and install it on your system. I personally am a fan of `FiraCode NF`.

_p.s. `NF` Stands for Nerd Font_

#### Oh-My-Posh

Launch a powershell with administrative privilleges and run these commands:

- `Install-Module posh-git`
- `Install-Module oh-my-posh`
- Then enable the default prompt using `Set-Prompt`.
- Change the theme to Powerlevel10k with `Set-Theme Powerlevel10k-Classic`.

Now we will need to finish up.

- Open the terminal's profile file using your code editor. I use VSCode. `code $PROFILE`. It acts as the rc file.
- Copy the contents of `Microsoft.PowerShell_profile.ps1` in it.

### Configuring Windows Terminal

Open your windows terminal. To open the settings file, either select settings under the **𝘃** sign, or by pressing `ctrl + ,`. These settings are applied automatically each time you save your file.
Once there, do the following:

> **Notes**:
>
> - Make sure you have anaconda set up on your system.
> - Be extra careful about tailing `,` and other stuff.

1. Under `profiles`, add my `defaults` segment to yours. These are mainly aesthetic and QOL changes.
2. Under `list`, copy and paste my conda powershell configurations. The order of items in this list is the order they will be displayed on your terminals list, so copy it on top 😁. Keep in mind that for the icon to work, you should get a `PNG` icon and add save it to `c:/ProgramData/Anaconda3/anaconda-icon.png`. You can use [this one](http://raden.fke.utm.my/_/rsrc/1540088596925/blog/anacondanavigatorlaunchericon/anaconda-icon-1024x1024.png?height=2000&width=2000).
3. You can go on and disable the prompts you don't need _like the `Azure Cloud Shell`_, by changing `hidden` to `true` under thier respective entries.
4. Now you can change your default terminal to the conda power shell by changing `defaultProfile` on line 11 to the `guid` of the conda terminal you just set up.

### Configuration

#### Default Starting Path

Is a code folder on your desktop for all terminals. You can change it under `"startingDirectory":` on line 35.

#### Default Conda Environment

My conda is set up to activate an environment named `sandox`. You can change this to any other environment of your choice, provided it is set up on your system beforehand. To do so, change the end of line 46 where it reads `conda activate sandbox` to your own environment you set up earlier.

#### Powerlevel10k Theme Settings

I didn't quite like the way things were set up on my theme so I moved them around. It's a bit tricky, but I'll try to explain. You can just go on and replace your code with mine, but **MAKE SURE TO HAVE A BACKUP BEFOREHAND**. Alright, first, we need to find where the theme file is located. run `$ThemeSettings.CurrentThemeLocation` while `Powerlevel10k-Classic` theme is enabled and open the file it shows you in a text editor. Something like:
`code ThemeSettings.CurrentThemeLocation`
will do. Compare it to my file, make the changes, you'll figure it out I don't feel like typing anymore. 😁

### Troubleshooting

#### Transparency Issues

Restart your computer, it'll be fine.

#### Weird Or Broken Fonts

Run `$ThemeSettings.PromptSymbols` and `$ThemeSettings.GitSymbols` to see all of the symbols the theme is using. Try other fonts from the repo above until they are fixed. You can change your font on line under `defaults` part of your `settings.json` under `fontFace`. Pay attention to capital letters and spaces in the name.

## TODO

- Automate the installation with a script.
